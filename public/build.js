function Note(ctrl) {
	this.exists = m.prop(false);
	this.notes;
	this.to_add = m.prop('');
	this.is_adding = m.prop(false);
	this.backup;
	this.init = () => {
		http_get('get_notes').then((data) => {
			this.exists(data.valid);
			console.log(data);
			this.notes = m.prop([]);
			if ( data.valid ) {
				for(let i = 0; i < data.data.length; i++) {
					this.notes().push({
						index:i, 
						id: data.data[i].id,
						content: m.prop(data.data[i].content),
						is_editing: m.prop(false)
					})
				}
			} else {
				this.notes([]);
			}
		})
	}

	this.init();

	this.enable_add = () => {
		this.is_adding(true);
	}

	this.add = () => {
		console.log(this.to_add());
		http_post('add_note', {content: this.to_add()})
			.then((data) => {
				display_response(data);
				if(data.valid) {
					this.is_adding(false);
					this.exists(true);
					this.init();
				}
			})
	}

	this.enable_edit = (i) => {
		this.backup = this.notes()[i].content();
		this.notes()[i].is_editing(true);
	}

	this.cancel_edit = (i) => {
		this.notes()[i].content(this.backup);
		this.notes()[i].is_editing(false);
	}

	this.edit = (i) => {
		http_post('edit_note', {
			id: this.notes()[i].id,
			content: this.notes()[i].content()
		}).then((data) => {
			display_response(data);
			if(data.valid) {
				this.notes()[i].is_editing(false);
			}
		})
	}

	this.remove = (i) => {
		http_post('remove_note', {id: this.notes()[i].id})
			.then((data) => {
				display_response(data);
				if (data.valid) {
					this.init();
				}
			})
	}
}
function NoteTable(ctrl) {
	return m('table.table', [
		m('thead', [
			m('tr', [
				m('th', 'Nummer'),
				m('th', 'Notiz')
			])
		]),
		m('tbody', [
			ctrl.note.exists() ?
				ctrl.note.notes().map((note) => {
					return m('tr', [
						m('td', note.index + 1),
						note.is_editing() ? [
								m('td', [
									m('input.form-control', {
										onchange: m.withAttr("value", note.content),
										value: note.content()})
								]),
								m('td', [
									m('button.btn.btn-success', {onclick: ctrl.note.edit.bind(ctrl, note.index)},
										'Speichern'),
									m('button.btn.btn-danger', {onclick: ctrl.note.cancel_edit.bind(ctrl, note.index)},
										'Abbrechen')
								])
							]
						: 
							[
								m('td', note.content()),
								m('td', [
									m('button.btn.btn-info', {onclick: ctrl.note.enable_edit.bind(ctrl, note.index)}, [
										m('i.glyphicon.glyphicon-pencil')
									]),
									m('button.btn.btn-danger', {onclick: ctrl.note.remove.bind(ctrl, note.index)}, [
										m('i.glyphicon.glyphicon-remove')
									])
								])
							]
					])
				})
			:
				layout = m('h3', 'Keine Notizen vorhanden')
		]),
		ctrl.note.is_adding() ?
			m('.form-group', [
				m('label', 'Notiz'),
				m('input.form-control', {
					onchange: m.withAttr("value", ctrl.note.to_add),
					value: ctrl.note.to_add()
				}),
				m('button.btn.btn-success', {onclick: ctrl.note.add}, 'Speichern')
			])
		:
			m('button.btn.btn-info', {onclick: ctrl.note.enable_add}, [
				m('i.glyphicon.glyphicon-plus'),
				m('span', 'Hinzufuegen')
			])
	])
}
function User() {
	this.exists = m.prop(false);
	this.is_editing = m.prop(false);
	this.f_name = m.prop('');
	this.s_name = m.prop('');
	this.university = m.prop('');
	this.img_url = m.prop('');
	this.img_temp = m.prop('');

	this.init = () => {
		http_get('get_user').then((data) => {
			this.exists(data.valid);
			if(data.valid) {
				this.f_name(data.data.f_name);
				this.s_name(data.data.s_name);
				this.university(data.data.university);
				this.img_url = BASE_URL() + 'profile.png';
			}
		});
	}
	this.enable_edit = () => {
		this.is_editing(true);
	}

	this.set = () => {
		let test = this.validate();
		if(test.valid) {
			http_post('set_user', {
				f_name: this.f_name(),
				s_name: this.s_name(),
				university: this.university()
			}).then((data) => {
				display_response(data);
			})
		} else {
			display_response(test)
		}
	}

	this.edit = () => {
		let test = this.validate();
		if(test.valid) {
			http_post('edit_user', {
				f_name: this.f_name(),
				s_name: this.s_name(),
				university: this.university()
			}).then((data) => {
				display_response(data);
				if (data.valid) {
					this.is_editing(false);
				}
			});
		} else {
			display_response(test)
		}
	}

	this.upload_img = () => {
		http_post_img('upload_img', this.img_temp())
			.then((data) => {
				console.log(data);
			})
	}

	this.validate = () => {
		var msg = ""
		if(this.f_name().length > 20)	 {
			msg += "\nVorname muss hoechstens 20 Zeichen lang sein";
		}
		if(this.s_name().length > 30) {
			msg += '\nNachname muss hoechstens 30 Zeichen lang sein';
		}
		if(this.university().length > 25) {
			msg += '\nUniversitaet muss hoechstens 25 Zeichen lang sein';
		}
		console.log(msg)
		return {valid: msg.length < 10 ,msg:msg}
	}
	this.init();
}


function UserTable(ctrl) {
	return m('#user-table', [
		m('table.table', [
			m('tbody', [
				m('tr', [
					m('td', 'Vorname'),
					ctrl.user.is_editing() ?
						m('td', [
							 m('input.form-control', {
								onchange: m.withAttr("value", ctrl.user.f_name),
								value: ctrl.user.f_name()
							}),
						])
					:
						m('td', ctrl.user.f_name())
				]),
				m('tr', [
					m('td', 'Nachname'),
					ctrl.user.is_editing() ?
						m('td', [
							 m('input.form-control', {
								onchange: m.withAttr("value", ctrl.user.s_name),
								value: ctrl.user.s_name()
							}),
						])
					:
						m('td', ctrl.user.s_name())
				]),
				m('tr', [
					m('td', 'Universitaet'),
					ctrl.user.is_editing() ?
						m('td', [
							 m('input.form-control', {
								onchange: m.withAttr("value", ctrl.user.university),
								value: ctrl.user.university()
							}),
						])
					:
						m('td', ctrl.user.university())
				])
			])
		]),
		ctrl.user.is_editing() ?
			m('button.btn.btn-success', {onclick: ctrl.user.edit}, 'Speichern')
		:
			m('button.btn.btn-info',{onclick: ctrl.user.enable_edit}, [
				m('i.glyphicon.glyphicon-pencil'),
				m('span', 'Bearbeiten')
			])
	])
}

function UserImg(ctrl) {
	return ('div', [
		m('img.img-responsive', {style: 'height:150px', src: ctrl.user.img_url}),
		m('label.btn.btn-info', [
			m('input.hidden', {type:'file', onchange: m.withAttr("value", ctrl.user.img_temp)}), 
			m('i.glyphicon.glyphicon-pencil'),
			m('span', 'Aendern')
		]),
		m('button.btn.btn-success', {onclick: ctrl.user.upload_img}, 'Speichern')
	])
}


function UserForm(ctrl) {
	return m('#user-form', [
				m('.form-group', [
					m('label', 'Vorname'),
					m('input.form-control', {
						onchange: m.withAttr("value", ctrl.user.f_name),
						value: ctrl.user.f_name()
					}),
				]),
				m('.form-group', [
					m('label', 'Nachname'),
					m('input.form-control', {
						onchange: m.withAttr("value", ctrl.user.s_name),
						value: ctrl.user.s_name()
					}),
				]),
				m('.form-group', [
					m('label', 'Universitaet'),
					m('input.form-control', {
						onchange: m.withAttr("value", ctrl.user.university),
						value: ctrl.user.university()
					}),
				]),
				m('button.btn.btn-success', {onclick: ctrl.user.set}, 'Fertig')
			])
}
function main() {
	let app = {
		controller: function() {
			this.user = new User()
			this.note = new Note()
		},
		view: function(ctrl) {
			return m('.container', [
				m('.page-header', [
					m('h1', 'Einsendeaufgabe 4')
				]),
				ctrl.user.exists() ? 
					m('div', [
						m('h1', 'Uebersicht'),
						m('.row', [
							m('.col-lg-8', [
								UserTable(ctrl)
							]),
							m('.col-lg-4', [
								UserImg(ctrl)
							])
						]),
						NoteTable(ctrl)
					])
				:	m('div', [
						m('h1', 'Registrierung'),
						UserForm(ctrl)
					])

			])
		}
	}
	m.mount(document.getElementById('container'), app)
}

main()

function BASE_URL() { return 'http://localhost/th_php_task_4/' }

function http_get(action) {
	let url = BASE_URL() + 'main.php?action=' + action;
	return m.request({method: 'GET', url: url});
}

function http_post(action, data) {
	let url = BASE_URL() + 'main.php?action=' + action;
	return m.request({method: 'POST', url: url, data: data});
}

function http_post_img(action, data) {
	let url = BASE_URL() + 'main.php?action=' + action;
	return m.request({method: 'POST', 
					url: url, data: data,
					serialize: function(value) {return value}});
}


function display_response(res) {
	let ele = document.getElementById('my-alert');
	if (ele) {
		ele.remove();
	}
	let element = document.createElement('div')
	element.id = 'my-alert';
	if(res.valid) {
		element.className = 'alert alert-success'
	} else {
		element.className = 'alert alert-danger'
	}
	element.style = 'text-align: center'
	element.innerHTML = res.msg
	document.body.insertBefore(element, document.body.firstChild)
}
