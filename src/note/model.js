function Note(ctrl) {
	this.exists = m.prop(false);
	this.notes;
	this.to_add = m.prop('');
	this.is_adding = m.prop(false);
	this.backup;
	this.init = () => {
		http_get('get_notes').then((data) => {
			this.exists(data.valid);
			console.log(data);
			this.notes = m.prop([]);
			if ( data.valid ) {
				for(let i = 0; i < data.data.length; i++) {
					this.notes().push({
						index:i, 
						id: data.data[i].id,
						content: m.prop(data.data[i].content),
						is_editing: m.prop(false)
					})
				}
			} else {
				this.notes([]);
			}
		})
	}

	this.init();

	this.enable_add = () => {
		this.is_adding(true);
	}

	this.add = () => {
		console.log(this.to_add());
		http_post('add_note', {content: this.to_add()})
			.then((data) => {
				display_response(data);
				if(data.valid) {
					this.is_adding(false);
					this.exists(true);
					this.init();
				}
			})
	}

	this.enable_edit = (i) => {
		this.backup = this.notes()[i].content();
		this.notes()[i].is_editing(true);
	}

	this.cancel_edit = (i) => {
		this.notes()[i].content(this.backup);
		this.notes()[i].is_editing(false);
	}

	this.edit = (i) => {
		http_post('edit_note', {
			id: this.notes()[i].id,
			content: this.notes()[i].content()
		}).then((data) => {
			display_response(data);
			if(data.valid) {
				this.notes()[i].is_editing(false);
			}
		})
	}

	this.remove = (i) => {
		http_post('remove_note', {id: this.notes()[i].id})
			.then((data) => {
				display_response(data);
				if (data.valid) {
					this.init();
				}
			})
	}
}
