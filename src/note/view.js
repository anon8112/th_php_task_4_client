function NoteTable(ctrl) {
	return m('table.table', [
		m('thead', [
			m('tr', [
				m('th', 'Nummer'),
				m('th', 'Notiz')
			])
		]),
		m('tbody', [
			ctrl.note.exists() ?
				ctrl.note.notes().map((note) => {
					return m('tr', [
						m('td', note.index + 1),
						note.is_editing() ? [
								m('td', [
									m('input.form-control', {
										onchange: m.withAttr("value", note.content),
										value: note.content()})
								]),
								m('td', [
									m('button.btn.btn-success', {onclick: ctrl.note.edit.bind(ctrl, note.index)},
										'Speichern'),
									m('button.btn.btn-danger', {onclick: ctrl.note.cancel_edit.bind(ctrl, note.index)},
										'Abbrechen')
								])
							]
						: 
							[
								m('td', note.content()),
								m('td', [
									m('button.btn.btn-info', {onclick: ctrl.note.enable_edit.bind(ctrl, note.index)}, [
										m('i.glyphicon.glyphicon-pencil')
									]),
									m('button.btn.btn-danger', {onclick: ctrl.note.remove.bind(ctrl, note.index)}, [
										m('i.glyphicon.glyphicon-remove')
									])
								])
							]
					])
				})
			:
				layout = m('h3', 'Keine Notizen vorhanden')
		]),
		ctrl.note.is_adding() ?
			m('.form-group', [
				m('label', 'Notiz'),
				m('input.form-control', {
					onchange: m.withAttr("value", ctrl.note.to_add),
					value: ctrl.note.to_add()
				}),
				m('button.btn.btn-success', {onclick: ctrl.note.add}, 'Speichern')
			])
		:
			m('button.btn.btn-info', {onclick: ctrl.note.enable_add}, [
				m('i.glyphicon.glyphicon-plus'),
				m('span', 'Hinzufuegen')
			])
	])
}
