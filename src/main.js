function main() {
	let app = {
		controller: function() {
			this.user = new User()
			this.note = new Note()
		},
		view: function(ctrl) {
			return m('.container', [
				m('.page-header', [
					m('h1', 'Einsendeaufgabe 4')
				]),
				ctrl.user.exists() ? 
					m('div', [
						m('h1', 'Uebersicht'),
						m('.row', [
							m('.col-lg-8', [
								UserTable(ctrl)
							]),
							m('.col-lg-4', [
								UserImg(ctrl)
							])
						]),
						NoteTable(ctrl)
					])
				:	m('div', [
						m('h1', 'Registrierung'),
						UserForm(ctrl)
					])

			])
		}
	}
	m.mount(document.getElementById('container'), app)
}

main()
