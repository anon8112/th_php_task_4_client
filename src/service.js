
function BASE_URL() { return 'http://localhost/th_php_task_4/' }

function http_get(action) {
	let url = BASE_URL() + 'main.php?action=' + action;
	return m.request({method: 'GET', url: url});
}

function http_post(action, data) {
	let url = BASE_URL() + 'main.php?action=' + action;
	return m.request({method: 'POST', url: url, data: data});
}

function http_post_img(action, data) {
	let url = BASE_URL() + 'main.php?action=' + action;
	return m.request({method: 'POST', 
					url: url, data: data,
					serialize: function(value) {return value}});
}


function display_response(res) {
	let ele = document.getElementById('my-alert');
	if (ele) {
		ele.remove();
	}
	let element = document.createElement('div')
	element.id = 'my-alert';
	if(res.valid) {
		element.className = 'alert alert-success'
	} else {
		element.className = 'alert alert-danger'
	}
	element.style = 'text-align: center'
	element.innerHTML = res.msg
	document.body.insertBefore(element, document.body.firstChild)
}
