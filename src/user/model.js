function User() {
	this.exists = m.prop(false);
	this.is_editing = m.prop(false);
	this.f_name = m.prop('');
	this.s_name = m.prop('');
	this.university = m.prop('');
	this.img_url = m.prop('');
	this.img_temp = m.prop('');

	this.init = () => {
		http_get('get_user').then((data) => {
			this.exists(data.valid);
			if(data.valid) {
				this.f_name(data.data.f_name);
				this.s_name(data.data.s_name);
				this.university(data.data.university);
				this.img_url = BASE_URL() + 'profile.png';
			}
		});
	}
	this.enable_edit = () => {
		this.is_editing(true);
	}

	this.set = () => {
		let test = this.validate();
		if(test.valid) {
			http_post('set_user', {
				f_name: this.f_name(),
				s_name: this.s_name(),
				university: this.university()
			}).then((data) => {
				display_response(data);
			})
		} else {
			display_response(test)
		}
	}

	this.edit = () => {
		let test = this.validate();
		if(test.valid) {
			http_post('edit_user', {
				f_name: this.f_name(),
				s_name: this.s_name(),
				university: this.university()
			}).then((data) => {
				display_response(data);
				if (data.valid) {
					this.is_editing(false);
				}
			});
		} else {
			display_response(test)
		}
	}

	this.upload_img = () => {
		http_post_img('upload_img', this.img_temp())
			.then((data) => {
				console.log(data);
			})
	}

	this.validate = () => {
		var msg = ""
		if(this.f_name().length > 20)	 {
			msg += "\nVorname muss hoechstens 20 Zeichen lang sein";
		}
		if(this.s_name().length > 30) {
			msg += '\nNachname muss hoechstens 30 Zeichen lang sein';
		}
		if(this.university().length > 25) {
			msg += '\nUniversitaet muss hoechstens 25 Zeichen lang sein';
		}
		console.log(msg)
		return {valid: msg.length < 10 ,msg:msg}
	}
	this.init();
}

