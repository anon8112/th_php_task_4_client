
function UserTable(ctrl) {
	return m('#user-table', [
		m('table.table', [
			m('tbody', [
				m('tr', [
					m('td', 'Vorname'),
					ctrl.user.is_editing() ?
						m('td', [
							 m('input.form-control', {
								onchange: m.withAttr("value", ctrl.user.f_name),
								value: ctrl.user.f_name()
							}),
						])
					:
						m('td', ctrl.user.f_name())
				]),
				m('tr', [
					m('td', 'Nachname'),
					ctrl.user.is_editing() ?
						m('td', [
							 m('input.form-control', {
								onchange: m.withAttr("value", ctrl.user.s_name),
								value: ctrl.user.s_name()
							}),
						])
					:
						m('td', ctrl.user.s_name())
				]),
				m('tr', [
					m('td', 'Universitaet'),
					ctrl.user.is_editing() ?
						m('td', [
							 m('input.form-control', {
								onchange: m.withAttr("value", ctrl.user.university),
								value: ctrl.user.university()
							}),
						])
					:
						m('td', ctrl.user.university())
				])
			])
		]),
		ctrl.user.is_editing() ?
			m('button.btn.btn-success', {onclick: ctrl.user.edit}, 'Speichern')
		:
			m('button.btn.btn-info',{onclick: ctrl.user.enable_edit}, [
				m('i.glyphicon.glyphicon-pencil'),
				m('span', 'Bearbeiten')
			])
	])
}

function UserImg(ctrl) {
	return ('div', [
		m('img.img-responsive', {style: 'height:150px', src: ctrl.user.img_url}),
		m('label.btn.btn-info', [
			m('input.hidden', {type:'file', onchange: m.withAttr("value", ctrl.user.img_temp)}), 
			m('i.glyphicon.glyphicon-pencil'),
			m('span', 'Aendern')
		]),
		m('button.btn.btn-success', {onclick: ctrl.user.upload_img}, 'Speichern')
	])
}


function UserForm(ctrl) {
	return m('#user-form', [
				m('.form-group', [
					m('label', 'Vorname'),
					m('input.form-control', {
						onchange: m.withAttr("value", ctrl.user.f_name),
						value: ctrl.user.f_name()
					}),
				]),
				m('.form-group', [
					m('label', 'Nachname'),
					m('input.form-control', {
						onchange: m.withAttr("value", ctrl.user.s_name),
						value: ctrl.user.s_name()
					}),
				]),
				m('.form-group', [
					m('label', 'Universitaet'),
					m('input.form-control', {
						onchange: m.withAttr("value", ctrl.user.university),
						value: ctrl.user.university()
					}),
				]),
				m('button.btn.btn-success', {onclick: ctrl.user.set}, 'Fertig')
			])
}
